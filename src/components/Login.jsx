import AccountCircleIcon from "@material-ui/icons/AccountCircle"
import LockIcon from "@material-ui/icons/Lock"
import PhoneIcon from "@material-ui/icons/Phone"
import Logo from "../assets/icons/Variant.png"


export default function Login() {

  return (
    <div className="h-screen flex font-body login-form">
      <div className="w-6/12 h-screen flex flex-col gap-5 justify-center items-center bg-gradient-to-t from-blue-100">
        <img src={Logo} alt="logo" style={{width: "60%"}} />
      </div>
      <div className="w-6/12 h-screen bg-sky-50 justify-around items-center flex flex-col shadow ">
        <div className="w-3/4 mt-24 rounded-2xl shadow-lg bg-white h-6/12 p-3">
          <div className="text-3xl font-semibold p-4">Войти в систему</div>
          <hr></hr>
          <form>
            <div className="flex flex-col p-6 font-semibold space-y-6">
              <div className="flex flex-col space-y-2 ">
                <label>Имя пользователья</label>
                <span className="flex items-center space-x-2 p-3 bg-background_2 rounded-lg form-item border">
                  <span>
                    <AccountCircleIcon style={{ color: "#6E8BB7" }} />
                  </span>
                  <input
                    className="w-full"
                    style={{outline: 'none'}}
                    placeholder="Введите имя пользователья"
                    type="text"
                    spellCheck="false"
                    id="login"
                  ></input>
                </span>
              </div>
              <div
                className='flex flex-col space-y-2'
              >
                <label>Пароль</label>
                <span className="items-center space-x-2 p-3 bg-background_2 rounded-lg flex form-item border">
                  <span>
                    <LockIcon style={{ color: "#6E8BB7" }} />
                  </span>
                  <input
                    style={{outline: 'none'}}
                    className="w-full"
                    type="password"
                    placeholder="Введите имя пользователья"
                    spellCheck="false"
                    id="password"
                  ></input>
                </span>
              </div>
            </div>
            <hr></hr>
            <div className="px-6 py-3">
              <button style={{backgroundColor: '#4094F7'}} className="w-full flex justify-center align-center border rounded p-3 text-white">Войти</button>
            </div>
          </form>
        </div>

        <div className="rounded rounded-xl  h-21 w-3/4 p-5 bg-white flex justify-between items-center shadow-lg">
          <div className="flex space-x-2 items-center">
            <PhoneIcon />
            <span>Служба поддержки</span>
          </div>
          <div>
            <span>+998 (99) 813 80 99</span>
          </div>
        </div>
      </div>
    </div>
  )
}
